﻿Namespace Models
    Public Class DbListResult 
        Inherits Result
        
        Public Sub New()
            DbList = new List(Of String)
        End Sub


        Property DbList As List(Of String)
    End Class
End NameSpace