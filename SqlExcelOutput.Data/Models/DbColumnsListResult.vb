﻿Namespace Models
    
    Public Class DbColumnsListResult
        Inherits Result
         Public Sub New()
            ColumnsList = new List(Of String)
        End Sub


        Property ColumnsList As List(Of String)
    End Class
End NameSpace