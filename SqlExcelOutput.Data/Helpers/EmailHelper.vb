﻿
Imports System.Net.Mail

Namespace Helpers
 

    Public Class EmailHelper
        Public Sub SendMailAttachment(ByVal From As String, _
                                      ByVal sendTo As String, ByVal Subject As String, _
                                      ByVal Body As String, _
                                      Optional ByVal AttachmentFile As String = "", _
                                      Optional ByVal CC As String = "", _
                                      Optional ByVal BCC As String = "", _
                                      Optional ByVal Host As String = "localhost", _
                                      Optional ByVal Port As Integer = 25, _
                                      Optional ByVal UserName As String = "", _
                                      Optional ByVal Password As String = "", _
                                      Optional ByVal EnableSsl As Boolean = False)

            Dim mail As New MailMessage()
            Dim SmtpServer As New SmtpClient(Host)
            mail.From = New MailAddress(From)
            mail.[To].Add(sendTo)
            mail.Subject = Subject
            mail.Body = Body
            mail.IsBodyHtml = True

            If Not String.IsNullOrEmpty(AttachmentFile) Then 
                Dim attachment As System.Net.Mail.Attachment
                attachment = New System.Net.Mail.Attachment(AttachmentFile)
                mail.Attachments.Add(attachment)
            End If
			

            SmtpServer.Host = Host
            SmtpServer.Port = Port
            SmtpServer.Credentials = New System.Net.NetworkCredential(UserName, Password)
            SmtpServer.EnableSsl = EnableSsl

            SmtpServer.Send(mail)
			 

        End Sub

     
        Private Function FileExists(ByVal FileFullPath As String) _
            As Boolean
            If Trim(FileFullPath) = "" Then Return False

            Dim f As New IO.FileInfo(FileFullPath)
            Return f.Exists

        End Function


    End Class
End NameSpace