Namespace Helpers
    Public Interface IDbHelper
        Function GetDatabases() As List(Of String)
        Function GetViews() As List(Of DbObject)
        Function GetSps() As List(Of DbObject)
        Function GetOutputColumnsOfView(viewName As String) As List(Of String)
        Function GetOutputColumnsOfSp(spName As String) As List(Of String)


    End Interface
End NameSpace