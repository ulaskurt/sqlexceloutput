﻿Imports System.Data.SqlClient

Namespace Helpers
    Public Class ServerDetails
        Public Sub New(dataSource As String, dbName as String, user As String, pass As String, isIntegrated As Boolean)
			Host = dataSource
			UserName = user
			Password = pass
			IsTrusted = isIntegrated
            Database = dbName
		End Sub

        Public Sub New ()

        End Sub
		Public Property Host As String

		Public Property UserName As String

		Public Property Password As String

		Public Property IsTrusted As Boolean

		Public Property Database As String

		Public Function GetConnectionString() As String

			Dim connection = New SqlConnectionStringBuilder() 
            connection.DataSource = Host
            
			If Not IsTrusted Then
				connection.UserID = UserName
				connection.Password = Password
			Else 
                connection.IntegratedSecurity = true
            End If


			If Not String.IsNullOrEmpty(Database) Then
				connection.InitialCatalog = Database
			End If

			Return connection.ToString()
		End Function
    End Class
End Namespace
