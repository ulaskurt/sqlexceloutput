﻿
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
Imports SqlExcelOutput.Models
Imports SqlExcelOutput.Web.Model

Namespace Helpers

Public Class DbHelper
    
        private ReadOnly _connectionString As String = ConfigurationManager.ConnectionStrings("MySqlConnection").ConnectionString

    
    Private Property _server As ServerDetails

    Public Sub New()
        
    End Sub

    Public Sub New(server As ServerDetails)
        _server = server
    End Sub



    Public Function GetUserSettings(userId As Guid) As Setting

        Dim setting  = New Setting()

        Using connection = New SqlConnection(_connectionString)
            connection.Open()
            Dim command = New SqlCommand()
            command.Connection = connection
            command.CommandType = CommandType.Text
            command.CommandText = "SELECT * FROM settings Where UserId = @UserId"

            command.Parameters.AddWithValue("@UserId", UserId)
            Dim dr = command.ExecuteReader(CommandBehavior.CloseConnection)

            While dr.Read()
                setting.Id = dr.GetInt32(dr.GetOrdinal("Id")) 
                setting.UserId = dr.GetGuid(dr.GetOrdinal("UserId")) 
                setting.Host = dr.GetString(dr.GetOrdinal("Host")) 
                setting.UserName = dr.GetString(dr.GetOrdinal("UserName")) 
                setting.Password = dr.GetString(dr.GetOrdinal("Password")) 
                setting.DataBaseName = dr.GetString(dr.GetOrdinal("DataBaseName")) 
                setting.EmailTo = dr.GetString(dr.GetOrdinal("EmailTo"))     
                    setting.ExcelTemplateFile= dr.GetString(dr.GetOrdinal("ExcelTemplateFile"))      
                    setting.SmtpHost = dr.GetString(dr.GetOrdinal("SMTP_HOST"))      
                    setting.SmtpUserName = dr.GetString(dr.GetOrdinal("SMTP_USERNAME"))      
                    setting.SmtpPassword = dr.GetString(dr.GetOrdinal("SMTP_PASSWORD"))      
                    setting.SmtpPort = dr.GetString(dr.GetOrdinal("SMTP_PORT"))      
                    setting.UseTls = dr.GetBoolean(dr.GetOrdinal("SMTP_USE_TLS"))      

            End While
        End Using

        Return setting
    End Function

        Public Function GetTemplateFile(userId As Guid, SpName As String) As String

        Dim setting  = New Setting()

        Dim templateFile as Object =""

        Using connection = New SqlConnection(_connectionString)
            connection.Open()
            Dim command = New SqlCommand()
            command.Connection = connection
            command.CommandType = CommandType.Text
            command.CommandText = "SELECT * FROM Templates Where UserId = @UserId And SpName=@SpName"

            command.Parameters.AddWithValue("@UserId", UserId)
            command.Parameters.AddWithValue("@SpName", SpName)

            Dim dr = command.ExecuteReader(CommandBehavior.CloseConnection)

            While dr.Read()
                templateFile  = dr.GetString(dr.GetOrdinal("ExcelFile"))                   
            End While
                 
        End Using

        Return templateFile 
    End Function

    Public Function GetDatabases() As List(Of String)  

        Dim databases = New List(Of String)()

        Using connection = New SqlConnection(_server.GetConnectionString())
            connection.Open()
            Dim command = New SqlCommand()
            command.Connection = connection
            command.CommandType = CommandType.Text
            command.CommandText = "SELECT name FROM master.sys.databases"

            Dim dr = command.ExecuteReader(CommandBehavior.CloseConnection)

            While dr.Read()
                databases.Add(dr("name").ToString())

            End While
        End Using

        Return databases
    End Function

    Public Function GetViews() As List(Of DbObject)  
        Dim objects = New List(Of DbObject)()

        Using connection = New SqlConnection(_server.GetConnectionString())
            connection.Open()

            Dim commandSps = New System.Data.SqlClient.SqlCommand()
            commandSps.Connection = connection
            commandSps.CommandType = CommandType.Text
            commandSps.CommandText = "SELECT name FROM sysobjects WHERE xtype = 'V' "

            Dim dr = commandSps.ExecuteReader(CommandBehavior.CloseConnection)

            While dr.Read()
                Dim obj = New DbObject()
                obj.Name = dr("name").ToString()
                obj.DbObjectType = DatabaseObjectType.StoredProcedure
                objects.Add(obj)
            End While
        End Using
        Return objects
    End Function

    Public Function GetSps() As List(Of DbObject)
        Dim objects = New List(Of DbObject)()

        Using connection = New SqlConnection(_server.GetConnectionString())
            connection.Open()

            Dim commandSps = New System.Data.SqlClient.SqlCommand()
            commandSps.Connection = connection
            commandSps.CommandType = CommandType.Text
            commandSps.CommandText = "SELECT name FROM sysobjects WHERE xtype = 'P' "

            Dim dr = commandSps.ExecuteReader(CommandBehavior.CloseConnection)

            While dr.Read()
                Dim obj = New DbObject()
                obj.Name = dr("name").ToString()
                obj.DbObjectType = DatabaseObjectType.StoredProcedure
                objects.Add(obj)


            End While
        End Using

        Return objects
    End Function


    Public Function GetOutputColumnsOfView(viewName As String) As List(Of String)
        Dim columns = New List(Of String)()

        Using connection = New SqlConnection(_server.GetConnectionString())
            connection.Open()

            Dim command = New System.Data.SqlClient.SqlCommand()
            command.Connection = connection
            command.CommandType = CommandType.Text
            command.CommandText = "SELECT COLUMN_NAME  FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @ViewName ORDER BY ORDINAL_POSITION ASC;"

            command.Parameters.AddWithValue("@ViewName", viewName)

            Dim dr = command.ExecuteReader(CommandBehavior.CloseConnection)

            While dr.Read()
                columns.Add(dr("COLUMN_NAME").ToString())
            End While
        End Using
        Return columns
    End Function


    Public Function GetOutputColumnsOfSp(spName As String) As List(Of String)
        Dim columns = New List(Of String)()

        Using connection = New SqlConnection(_server.GetConnectionString())
            connection.Open()

            Dim command = New System.Data.SqlClient.SqlCommand()
            command.Connection = connection
            command.CommandType = CommandType.Text
            command.CommandText = "SELECT name, system_type_name FROM sys.dm_exec_describe_first_result_set_for_object (OBJECT_ID(@SpName),  NULL); "

            command.Parameters.AddWithValue("@SpName", spName)

            Dim dr = command.ExecuteReader(CommandBehavior.CloseConnection)

            While dr.Read()
                columns.Add(dr("name").ToString())
            End While
        End Using
        Return columns
    End Function

     Public Function ReturnResults(Id As Integer, spName As String, email As String, Starts As DateTime, Ends As DateTime, ADate as DateTime ) As DataTable
           Dim ds As new DataSet

        Using connection = New SqlConnection(_server.GetConnectionString())
            connection.Open()

            Dim command = New System.Data.SqlClient.SqlCommand()
            command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = SpName


                If (SpName.EndsWith("DATE_SPECIFIC_DATE"))
                    command.Parameters.AddWithValue("@ADate", ADate)
                End If
                If (SpName.EndsWith("DATES_BETWEEN_DATES"))
                    command.Parameters.AddWithValue("@StartDate", Starts)
                    command.Parameters.AddWithValue("@EndDate", Ends)
                End If
                If (SpName.EndsWith("SPECIFIC_INTEGER"))
                    command.Parameters.AddWithValue("@Id", Id)
                End If


                Dim daSql As New SqlDataAdapter()                
             daSql.SelectCommand = command
                
          
             daSql.Fill(ds)

               
        End Using
        Return ds.Tables(0)
    End Function

    Public Sub SetServer(server As ServerDetails)
        
            _server = server
    End Sub
 
          Public Function GetPasswordResetToken(ByVal TokenString As String) As PasswordResetToken
        
            Dim token as PasswordResetToken = nothing


             Using connection = New SqlConnection(_connectionString)
            connection.Open()
            Dim command = New SqlCommand()
            command.Connection = connection
            command.CommandType = CommandType.Text
            command.CommandText = "SELECT * FROM PasswordResetTokens Where Token = @Token"

            command.Parameters.AddWithValue("@Token", TokenString)
            Dim dr = command.ExecuteReader(CommandBehavior.CloseConnection)

            If dr.Read()
                token = New PasswordResetToken()
                token.Id = dr.GetInt32(dr.GetOrdinal("Id")) 
                token.UserId = dr.GetGuid(dr.GetOrdinal("UserId")) 
                token.IsActive = dr.GetBoolean(dr.GetOrdinal("IsActive")) 
                token.DateCreated = dr.GetDateTime(dr.GetOrdinal("DateCreated")) 
                token.Token = dr.GetString(dr.GetOrdinal("Token"))             
            End If 


            return token

        End Using

    End Function
        Public Function InsertPasswordResetToken(ByVal TokenString As String, UserId as Guid ) As string
        
            Dim token as PasswordResetToken = nothing


             Using connection = New SqlConnection(_connectionString)
                connection.Open()
                Dim command = New SqlCommand()
                command.Connection = connection
                command.CommandType = CommandType.Text
                command.CommandText = "Insert Into PasswordResetTokens(UserId,Token,IsActive)  Values(@UserId,@Token,@IsActive)"

                command.Parameters.AddWithValue("@UserId", UserId)
                command.Parameters.AddWithValue("@Token", TokenString)
                command.Parameters.AddWithValue("@ISActive", True)

                command.ExecuteNonQuery()
                connection.Close()
                

             End Using
            
            return TokenString
    End Function

         Public Function UpdatePasswordResetToken(ByVal TokenString As String) As string
        
            Dim token as PasswordResetToken = nothing


             Using connection = New SqlConnection(_connectionString)
                connection.Open()
                Dim command = New SqlCommand()
                command.Connection = connection
                command.CommandType = CommandType.Text
                command.CommandText = "UPDATE PasswordResetTokens SET IsActive = 1 WHERE Token=@Token"

                
                command.Parameters.AddWithValue("@Token", TokenString)
                
                command.ExecuteNonQuery()
                connection.Close()
                

             End Using
            
            return TokenString
    End Function

End Class
End Namespace
