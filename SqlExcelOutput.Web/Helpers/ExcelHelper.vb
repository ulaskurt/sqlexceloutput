﻿
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports OfficeOpenXml
Imports OfficeOpenXml.Table

Namespace Helpers
Public Class ExcelHelper

        Public Function GetExcelFile(template as String, dt as DataTable) As String

            Dim name As String = Guid.NewGuid().ToString()
            Dim templateFilePath As String = HttpContext.Current.Server.MapPath(template)
            Dim templateFile As New FileInfo(templateFilePath)

            Dim newFilePth As String = HttpContext.Current.Server.MapPath("~") + "Output\" + name + ".xlsx"
            Dim newFile As New FileInfo(newFilePth)
            
            Using package As New ExcelPackage(newFile, templateFile)
	            ' Openning first Worksheet of the template file i.e. 'Sample1.xlsx'
	           
				'For i As Integer = 1 To package.Workbook.Worksheets.Count
				'	If(Not i = 1)
				'		package.Workbook.Worksheets.Delete(i)
				'	End If	                    
				'Next
				
                'package.Workbook.Worksheets.Delete(2)

				package.Workbook.Worksheets.Add("Data")
				package.Workbook.Worksheets.MoveToEnd("Data")

                Dim worksheet As ExcelWorksheet = package.Workbook.Worksheets(package.Workbook.Worksheets.Count)
                worksheet.Cells("A1").LoadFromDataTable(dt, True, TableStyles.Light1)

                package.Save()
            End Using

            Return newFilePth
        End Function

End Class

End Namespace