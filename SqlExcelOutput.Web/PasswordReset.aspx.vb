﻿ 
Imports SqlExcelOutput.Data.Helpers
Imports SqlExcelOutput.Data.Models


Public Class PasswordReset
    Inherits System.Web.UI.Page

    
    Public token as  PasswordResetToken 
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim tokenString as String = Request.QueryString("token")

        If Not String.IsNullOrEmpty(tokenString) Then
            Dim dbHelper as New DbHelper()
            token = dbHelper.GetPasswordResetToken(tokenString)

            pnlSent.Visible = false 

            If token Is Nothing Then
                pnlInvalidToken.Visible = true
                pnlReset.Visible = false 
            Else 
                pnlInvalidToken.Visible = false
                pnlReset.Visible = true 
            End If            
        End If

    End Sub

    Protected Sub cmdReset_Click(sender As Object, e As EventArgs) Handles cmdReset.Click
        Dim password as String = txtPassword.Text 
        Dim repeat as String = txtRepeat.Text

        If password = repeat Then
            
            Dim currentUser as MembershipUser = Membership.GetUser(token.UserId)
            Dim oldPassword = currentUser.GetPassword()
            currentUser.ChangePassword(oldPassword, password)
            
            Msg.Text =" Password Reset"
            pnlSent.Visible = true
        Else 
            Msg.Text =" Password and repeat must be same"
            pnlSent.Visible = False 
        End If

    End Sub
End Class