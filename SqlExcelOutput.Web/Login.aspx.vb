﻿Imports SqlExcelOutput.Data.Helpers


Public Class Login
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        ' Membership.CreateUser("ulas", "123456", "chris@bluedoorsoftware.co.uk")
        ' Membership.CreateUser("chris1", "chris123456!", "chris1@gmail.com")
        'Membership.CreateUser("chris2", "chris123456!", "chris2@gmail.com")
        'Membership.CreateUser("chris3", "chris123456!", "chris3@gmail.com")
        'Membership.CreateUser("chris4", "chris123456!", "chris4@gmail.com")

    End Sub

    Protected Sub Logon_Click(sender As Object, e As EventArgs) Handles cmdLogin.Click

        Dim user As String = UserEmail.Text.Trim()
        Dim pass As String = UserPass.Text.Trim()

        If Membership.ValidateUser(user, pass) Then

            FormsAuthentication.SetAuthCookie(user, False)

            FormsAuthentication.RedirectFromLoginPage(user, False)
            ' do something else
        Else

        End If
    End Sub



    <System.Web.Services.WebMethod()>
    Public Shared Function ForgotPassword(email As String) As Boolean


        Dim userName As String = Membership.GetUserNameByEmail(email)

        If Not String.IsNullOrEmpty(userName) Then
            Try
                Dim token As String = Guid.NewGuid().ToString()
                Dim link As String = HttpContext.Current.Request.Url.Authority & "/ForgotPassword.aspx?token=" + token
                Dim message As String = "Your pasword reset link is <a href=" & link & ">" & link & "</a>"
                Dim dbHelper As New DbHelper()

                Dim user As MembershipUser = Membership.GetUser(userName)

                Dim userId As Guid = New Guid(user.ProviderUserKey.ToString())
                dbHelper.InsertPasswordResetToken(token, userId)

                Dim mailHelper As New EmailHelper
                mailHelper.SendMailAttachment("chris@bluedoorsoftware.co.uk", email, "Password Reset", message)
                Return True
            Catch ex As Exception
              '  Throw New Exception("Error sending email" + ex.ToString())
                
            End Try
        Else 
            ' Throw New Exception("User not found")
        End If
                Return False
    End Function



End Class

