﻿ 
Imports SqlExcelOutput.Data.Helpers
Imports SqlExcelOutput.Data.Models
Imports SqlExcelOutput.Web.Models
Imports SqlExcelOutput.Web.Helpers

Public Class _Default
    Inherits System.Web.UI.Page

    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        cboSps.Attributes.Add("onChange","ShowParameters();")
        Dim dbHelper as new DbHelper()

        Dim userId  as String = HttpContext.Current.User.Identity.Name
        Dim user As MembershipUser = Membership.GetUser()

        Dim setting as Setting = dbHelper.GetUserSettings(user.ProviderUserKey)
        Dim server as New ServerDetails(setting.Host,setting.DataBaseName ,setting.UserName ,setting.Password ,False)

        dbHelper.SetServer(server)

        Dim spList As List(Of DbObject) = dbHelper.GetSps()
        Dim viewList As List(Of DbObject) = dbHelper.GetViews()
        
        Dim results as new DbObjectListResult
        results.ViewList = viewList
        results.SpList = spList
        
        'cboViews.DataSource = results.ViewList
        'cboViews.DataTextField = "name"
        'cboViews.DataValueField = "name"
        'cboViews.DataBind()

        cboSps.DataSource = results.SpList.Where(Function(x) x.Name.StartsWith("up_dataset_")).OrderBy(Function(x) x.Name)
        cboSps.DataTextField = "name"
        cboSps.DataValueField = "name"
        cboSps.DataBind()

        txtEmails.InnerText = setting.EmailTo

        cboSps.Items.Insert(0, " ")

    End Sub
    
'<System.Web.Services.WebMethod()> _
'Public Shared Function GetCurrentTime( ) As String
'   Return "Hello " &  "The Current Time is: " & _
'            DateTime.Now.ToString()
'End Function

'<System.Web.Services.WebMethod()> _
'Public Shared Function ConnectToDb(host As String,  userName As String, passWord  as String) As DbListResult


'        Dim server as New ServerDetails(host,"",userName,passWord ,False)
'        Dim dbHelper as new DbHelper(server)
'        Dim dbList As List(Of String) = dbHelper.GetDatabases()
        
'        Dim results as new DbListResult
'        results.DbList = dbList
        
'        Return results
'End Function
    
'<System.Web.Services.WebMethod()> _
'Public Shared Function GetDbObjects(host As String,  userName As String, passWord  as String, dataBase As String) As DbObjectListResult


'        Dim server as New ServerDetails(host,dataBase,userName,passWord ,False)
'        Dim dbHelper as new DbHelper(server)
'        Dim spList As List(Of DbObject) = dbHelper.GetSps()
'        Dim viewList As List(Of DbObject) = dbHelper.GetViews()
        
'        Dim results as new DbObjectListResult
'        results.ViewList = viewList
'        results.SpList = spList
        
'        Return results
'End Function
   
'<System.Web.Services.WebMethod()> _
'Public Shared Function GetOutputColumns(server as ServerDetails, name As String, objectType as string ) As DbColumnsListResult
        
'        Dim dbHelper as new DbHelper(server)

'        Dim dbList As New List(Of String) 
'        If(objectType = "view")
'            dbList = dbHelper.GetOutputColumnsOfView(name)
'         Else 
'            dbList = dbHelper.GetOutputColumnsOfSp(name)
'        End If
        
        
'        Dim results as new DbColumnsListResult
'        results.ColumnsList = dbList
        
'        Return results
'End Function
    
    
<System.Web.Services.WebMethod()> _
Public Shared Sub LogOut() 
         FormsAuthentication.SignOut()
End Sub


<System.Web.Services.WebMethod()> _
Public Shared Function SendObjectResults(Id as Integer,SpName As String,Email as string, Starts As DateTime, Ends As DateTime, ADate as DateTime ) As Result
        Dim dbHelper as new DbHelper()        
        Dim user As MembershipUser = Membership.GetUser()

        Dim setting as Setting = dbHelper.GetUserSettings(user.ProviderUserKey)
        Dim server as New ServerDetails(setting.Host,setting.DataBaseName ,setting.UserName ,setting.Password ,False)

        dbHelper.SetServer(server) 
       
        Dim results As DataTable = dbHelper.ReturnResults(Id,SpName, Email,Starts, Ends, ADate)
        
        Dim excelHelper As New ExcelHelper
        Dim template As String = dbHelper.GetTemplateFile((New Guid(user.ProviderUserKey.ToString())) ,SpName)
        If String.IsNullOrEmpty(template)
            template = setting.ExcelTemplateFile
        End If

        Dim templateFilePath As String = HttpContext.Current.Server.MapPath(template)
        Dim rootPath As String = HttpContext.Current.Server.MapPath("~")
        Dim file as String = excelHelper.GetExcelFile(templateFilePath,rootPath, results)

        Dim mailHelper as New EmailHelper
        Dim from As String = "noreply@bluedoorsoftware.co.uk"
        Dim subject as String = "Report"
        Dim body as String = "see attached report"

        Dim emailAddress() As String
        Dim arg() As String = {";"}

        emailAddress = setting.EmailTo.Split(arg, StringSplitOptions.None)

        For i As Integer = 0 To emailAddress.Count -1
            'mailHelper.SendMailAttachment(from, emailAddress(i), subject, body, file,,,,)
            mailHelper.SendMailAttachment(from, emailAddress(i), subject, body, file, ,, setting.SmtpHost, setting.SmtpPort, setting.UserName, setting.Password, setting.UseTls)
        Next
        
        Dim sendResults as new Result
        
        
        Return Nothing
End Function
End Class