Imports SqlExcelOutput.Helpers
Imports SqlExcelOutput.Web.Helpers

Namespace Models
    Public Class DbObjectListResult 
        Inherits Result
        
        Sub New ()
            ViewList = New List(Of DbObject)()
            SpList = New List(Of DbObject)()
        End Sub

        Property ViewList As List(Of DbObject)

        Property SpList As List(Of DbObject)

    End Class
End NameSpace