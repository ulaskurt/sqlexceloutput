﻿<%@ Page Title="" Language="vb" AutoEventWireup="false"  
    CodeBehind="Login.aspx.vb" Inherits="SqlExcelOutput.Web.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"
			  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
			  crossorigin="anonymous"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link id="stylesheet" href="/scripts/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet">
    <script type="text/javascript" src="/scripts/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="http://ajax.cdnjs.com/ajax/libs/json2/20110223/json2.js"></script>

    <script src="scripts/app.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        
            <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Forgot Password?</h4>
                    </div>
                    <div class="modal-body">
                        <p>Please enter the email you used</p>
                        
                    <div class="form-group">
                        <label>Email</label>
                        <asp:TextBox ID="txtForgotPassword" runat="server" class="form-control" />
                         
                    </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="ForgotPassword()" >Send</button>
                    </div>
                </div>

            </div>
        </div>
            <div class="row" style="margin-top: 75px;">
                <div class="col-sm-4">
                    <h3>Log In</h3>

                    <div class="form-group">
                        <label>User Name</label>
                        <asp:TextBox ID="UserEmail" runat="server" class="form-control" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                            ControlToValidate="UserEmail"
                            Display="Dynamic"
                            ErrorMessage="Cannot be empty."
                            runat="server" />
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <asp:TextBox ID="UserPass" runat="server" class="form-control" TextMode="Password" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                            ControlToValidate="UserPass"
                            ErrorMessage="Cannot be empty."
                            runat="server" />
                    </div>
                    <div class="form-group">
                        <asp:Button ID="cmdLogin" OnClick="Logon_Click" Text="Log On" runat="server" class="btn btn-default" />

                        <asp:Label ID="Msg" ForeColor="red" runat="server" />

                    </div>

                </div>
                <button type="button" class="btn btn-info btn-sm" data-toggle="modal" onclick="forgotPassword"
                     data-target="#myModal">Forgot Password?</button>

            </div>

    </form>
</body>
</html>
