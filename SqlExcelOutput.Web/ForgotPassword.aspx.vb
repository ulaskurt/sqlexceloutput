﻿Imports SqlExcelOutput.Data.Helpers
Imports SqlExcelOutput.Data.Models
Imports SqlExcelOutput.Web.Helpers
Imports SqlExcelOutput.Web.Model

Public Class ForgotPassword
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dbHelper As New DbHelper()
        Dim tokenString As String = Request.QueryString("token")
        If Not String.IsNullOrEmpty(tokenString)
            Dim token As PasswordResetToken  = dbHelper.GetPasswordResetToken(tokenString)
            If DateTime.Now.Subtract(token.DateCreated).Days > 1 ANd Not token.IsActive  Then
                lblMessage.Text= "Your password reset requst has expired."
            Else 
                Dim resetUser As MembershipUser = Membership.GetUser(token.UserId)
                Dim newPassword as String = Membership.GeneratePassword(6,1)

                resetUser.ChangePassword(resetUser.GetPassword(), newPassword )
                lblMessage.Text= "Your password has changed to: " + newPassword

                dbHelper.UpdatePasswordResetToken(tokenString)
            End If
        Else 
             lblMessage.Text= "Invalid Request"  

        End If
        

    End Sub

End Class