﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master"
    CodeBehind="Default.aspx.vb" Inherits="SqlExcelOutput.Web._Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row" style="margin-top: 75px;">

        <div class="col-md-6">

            <fieldset>

                <!-- Form Name -->
                <legend>Select</legend>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="selectbasic">Stored Procedures</label>
                    <div class="col-md-4">
                        <asp:DropDownList ID="cboSps" runat="server" class="form-control">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group" id="divDate" style="display: none">
                    <label class="col-md-4 control-label" for="textinput">A Date</label>
                    <div class="col-md-4">
                        <input type="text" id="txtADate" class="form-control">
                        <span class="help-block">Please select a date</span>
                    </div>
                </div>
                <div class="form-group" id="divStarts" style="display: none">
                    <label class="col-md-4 control-label" for="textinput">Start Date</label>
                    <div class="col-md-4">
                        <input type="text" id="txtStarts" />
                        <span class="help-block">Please select a start date</span>
                    </div>
                </div>

                <div id="divEnds" class="form-group" style="display: none">
                    <label class="col-md-4 control-label" for="textinput">End Date</label>
                    <div class="col-md-4">
                        <input type="text" id="txtEnds" />
                        <span class="help-block">Please select an end  date</span>
                    </div>
                </div>


                <div class="form-group" id="divId" style="display: none">
                    <label class="col-md-4 control-label" for="textinput">Id</label>
                    <div class="col-md-4">
                        <input type="text" id="txtId" />
                        <span class="help-block">help</span>
                    </div>
                </div>



            </fieldset>
        </div>
        <div class="col-md-6">

            <fieldset>

                <legend>Send</legend>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="">Email Addreses</label>
                    <div class="col-md-4">
                        <div class="input-group">
                            <textarea id="txtEmails" runat="server" cols="20" rows="2" class="form-control"></textarea>

                        </div>

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        <div class="input-group">
                             

                            <button type="button" onclick="SendObjectResults();" 
                                 Text="Send as Excel" class="btn btn-default" >Send as email</button>

                        </div>

                    </div>
                </div>

            </fieldset>
        </div>
    </div>
</asp:Content>
