﻿
function toggleCredentials() {
    $("#divCredentials").toggle();
}

function ShowCurrentTime() {
    //alert("time");
    $.ajax({
        type: "POST",
        url: "Default.aspx/GetCurrentTime",
        //data: '{server: "' + $("#<%=txtUserName.ClientID%>")[0].value + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess,
        failure: function(response) {
            alert(response.d);
        }
    });
}

$('#txtADate').datepicker({
});

function Logout() {


   
    $.ajax({
        type: "POST",
        url: "Default.aspx/Logout",
        data: {},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            window.location.replace("Login.aspx");
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function ShowParameters() {
    var spName = $("#MainContent_cboSps").val();

    //alert(spName);

    $("#divStarts").hide();
    $("#divEnds").hide();
    $("#divId").hide();
    $("#divDate").hide();

    if (spName.indexOf("CLIENTS_ALL") > -1) {
        // do nothing 
    }

    if (spName.indexOf("DATE_SPECIFIC_DATE") > -1) {
        $("#divDate").show();
    } else {
        $("#divDate").hide();
    }

    if (spName.indexOf("DATES_BETWEEN_DATES") > -1) {
        $("#divStarts").show();
        $("#divEnds").show();
    }  

    if (spName.indexOf("SPECIFIC_INTEGER") > -1) {
        $("#divId").show();       
    }  
     
}

function SendObjectResults() {

    if ($("#MainContent_cboSps").val() == "") {
        alert("Please select the sp to execute");
        return;
    }


    var spName = $("#MainContent_cboSps").val();
    var post = {};
    post.SpName = spName;
    post.Email = $("#MainContent_txtEmails").val();
    post.ADate = new Date();
    post.Starts = new Date();
    post.Ends = new Date();
    post.Id = -1;

    if (spName.indexOf("CLIENTS_ALL") > -1) {        
    }

    if (spName.indexOf("DATE_SPECIFIC_DATE") > -1) {
        if ($("#txtADate").val().trim() != "") {
            post.ADate = $("#txtADate").val();
        } else {
            alert("Please enter a date");
            return;
        }
        
    }

    if (spName.indexOf("DATES_BETWEEN_DATES") > -1) {
        if ($("#txtStarts").val().trim() != "")
            post.Starts = $("#txtStarts").val();
        else {
            alert("Please enter a start date");
            return;
        }

        if ($("#txtEnds").val().trim() != "")
            post.Ends = $("#txtEnds").val();
        else {
            alert("Please enter an end date");
            return;
        }
        

       
    }

    if (spName.indexOf("SPECIFIC_INTEGER") > -1) {
        if ($("#txtId").val().trim() != "")
            post.Id = $("#txtId").val();
        else {
            alert("Please enter a Id");
            return;
        }
        
        
    }

    var dataJsonString = JSON.stringify({ "Id": "-1", "SpName": post.SpName, "Email": post.Email, "ADate": post.ADate, "Starts": post.Starts, "Ends": post.Ends });

    //alert(dataJsonString);
    $.ajax({
        type: "POST",
        url: "Default.aspx/SendObjectResults",
        data: dataJsonString,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            alert("Report sent as email");            
        },
        failure: function (response) {
            alert("An error occured. Excel file could not sent");
        }
    });
}



function ForgotPassword() {

    var email = $("#txtForgotPassword").val();

    if (email == '') {
        alert("Please enter the email ");
        return;
    }


    var dataJsonString = JSON.stringify({ "email": email});
    $.ajax({
        type: "POST",
        url: "Login.aspx/ForgotPassword",
        data: dataJsonString,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if(response)
                alert("Passworkd Email Sent");
            else 
                alert("An eror occured while sending. Please try again.");
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}