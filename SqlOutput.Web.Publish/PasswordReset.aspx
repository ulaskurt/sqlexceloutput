﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PasswordReset.aspx.vb" Inherits="SqlExcelOutput.Web.PasswordReset" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            s<asp:Panel runat="server" ID="pnlInvalidToken">
                Your Request is invalid
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlReset">
            <div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">Password </label>
                    <div class="col-md-4">
                        <asp:TextBox runat="server" id="txtPassword" cssclass="form-control input-md"></asp:TextBox>
                        
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="passwordinput">Repeat Password </label>
                    <div class="col-md-4">
                        <asp:TextBox runat="server" id="txtRepeat" cssclass="form-control input-md"></asp:TextBox>
                    </div>
                </div>
 
                
                <div class="form-group">
                    <asp:Button ID="cmdReset" Text="Reset Password" runat="server" class="btn btn-default" />

                    <asp:Label ID="Msg" ForeColor="red" runat="server" />

                </div>

            </div>
            </asp:Panel>
            
            <asp:Panel runat="server" id="pnlSent">
            <h1>Password Reset</h1>

            <p>Your password reset. Please click <a href="login.aspx">here</a> to login</p>
                </asp:Panel>
          

        </div>
    </form>
</body>
</html>
