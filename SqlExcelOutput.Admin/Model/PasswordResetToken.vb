﻿Namespace Model
    Public Class PasswordResetToken

        Public Property Id As Integer

       Public Property UserId As Guid

       Public Property Token As String

       Public Property DateCreated As DateTime

       Public Property IsActive As Boolean

    End Class
End NameSpace