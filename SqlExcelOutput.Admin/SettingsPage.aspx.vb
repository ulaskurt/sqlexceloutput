﻿Imports SqlExcelOutput.Data.Helpers
Imports SqlExcelOutput.Data.Models

Public Class SettingsPage
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
         Dim dbHelper As New DbHelper()
       
        Dim userName As String = Request.QueryString("userName")
        Dim myUser as MembershipUser = Membership.GetUser(userName)
        Dim userId as Guid =  New Guid(myUser.ProviderUserKey.ToString())
        Dim settings as Setting = dbHelper.GetUserSettings(userId)

        If NOT settings Is Nothing
             
        
            txtHost.Text= settings.Host
            txtUserName.Text = settings.UserName
             txtPassword.Text = settings.Password 
             txtDataBaseName.Text = settings.DataBaseName
             txtEmailTo.Text = settings.EmailTo     
             txtExcelTemplateFile.Text= settings.ExcelTemplateFile      
            txtCustomerName.Text =settings.CustomerName
             txtSmtpHost.Text =settings.SmtpHost
             txtSmtpUserName.Text = settings.SmtpUserName
             txtSmtpPassword.Text = settings.SmtpPassword
             txtSmtpPort.Text = settings.SmtpHost
             chkSmtpUseTls.Checked = settings.UseTls
        End If

        Dim templates = dbHelper.GetTemplates(userId)

        if(templates.Count > 0) Then
            pnlNoData.Visible = False
            rptTemplates.DataSource = templates
            rptTemplates.DataBind()
        Else 
            pnlNoData.Visible = true    
        End If
        


    End Sub


    
     <System.Web.Services.WebMethod()> _
    Public Shared Function SaveSettings(settings As SqlExcelOutput.Data.Models.Setting) as Boolean

        Dim dbHelper As New DbHelper()
        
        Dim myUser as MembershipUser = Membership.GetUser(settings.UserName)
        Dim setting as Setting = dbHelper.GetUserSettings( New Guid(myUser.ProviderUserKey.ToString()))
        Dim isAdding as Boolean = false 
        if setting Is Nothing
            setting = New Setting()
            isAdding = True
       
        End If
        
        
            
        setting.UserId = New Guid(myUser.ProviderUserKey.ToString())
        setting.Host = settings.Host
        setting.UserName = settings.UserName
        setting.Password = settings.Password 
        setting.DataBaseName = settings.DataBaseName
        setting.EmailTo = settings.EmailTo     
        setting.CustomerName = settings.CustomerName
        setting.ExcelTemplateFile= settings.ExcelTemplateFile      
        setting.SmtpHost =settings.SmtpHost
        setting.SmtpUserName = settings.SmtpUserName
        setting.SmtpPassword = settings.SmtpPassword
        setting.SmtpPort = settings.SmtpHost
        setting.UseTls = settings.UseTls

        If(isAdding)
            dbHelper.InsertSetting(setting)
        Else 
            dbHelper.UpdateSetting(setting)
        End If
            
         Return true 
    End Function


    
    <System.Web.Services.WebMethod()> _
    Public Shared Function CreateTemplate(SpName As String, ExcelFile As String, UserName as String) as Boolean

          
            Dim template As New Template()
            template.SpName = SpName
            template.ExcelFile = ExcelFile

            Dim myUser as MembershipUser = Membership.GetUser(userName)
            template.UserId = New Guid(myUser.ProviderUserKey.ToString())

            Dim dbHelper as New DbHelper()
            dbHelper.InsertTemplate(template)
            
            

        Return True
         
    End Function

       
    <System.Web.Services.WebMethod()> _
    Public Shared Sub DeleteTemplate(Id as Integer) 

        Dim dbHelper as New DbHelper
        dbHelper.DeleteTeplate(Id)

    End Sub

End Class