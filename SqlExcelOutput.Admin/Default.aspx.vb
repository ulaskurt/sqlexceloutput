﻿Imports OfficeOpenXml.FormulaParsing.Excel.Functions.Logical
Imports SqlExcelOutput.Data.Helpers
Imports SqlExcelOutput.Data.Models

Public Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim users As MembershipUserCollection = Membership.GetAllUsers()
        rptUsers.DataSource = users        
        rptUsers.DataBind()
    End Sub
    

    <System.Web.Services.WebMethod()> _
    Public Shared Function CreateUser(UserName as String,Password As String, Email As String) as Boolean

            Membership.CreateUser(UserName, password, email)
        Return True
         
    End Function

    
    <System.Web.Services.WebMethod()> _
    Public Shared Function DeleteUser(UserName as String) as Boolean

            Dim myUser = Membership.GetUser(UserName)
        If Not myUser Is Nothing
            Membership.DeleteUser(myUser.UserName)
        End If
            
         Return true 
    End Function



End Class