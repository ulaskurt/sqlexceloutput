﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin.Master" CodeBehind="Default.aspx.vb" Inherits="SqlExcelOutput.Admin._Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row" style="margin-top: 75px;">

        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">User Details</h4>
                    </div>
                    <div class="modal-body">
                        <fieldset>
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">User name</label>
                                <div class="col-md-4">
                                    <input id="txtUserName" name="textinput" 
                                        class="form-control input-md" type="text">                                    
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="passwordinput">Email address</label>
                                <div class="col-md-4">
                                    <input id="txtEmail"  class="form-control input-md" >
                                   
                                </div>
                            </div>

                            <!-- Password input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="passwordinput">Password</label>
                                <div class="col-md-4">
                                    <input id="txtPassword" name="passwordinput" class="form-control input-md" type="password">
                                   
                                </div>
                            </div>

                        </fieldset>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="CreateUser()">Save</button>
                    </div>
                </div>

            </div>
        </div>
        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">Add new user</button>
        

        <h1>Users </h1>
        
        
        

        <asp:Repeater ID="rptUsers" runat="server">
            <HeaderTemplate>
                <table class="table table-striped">

                    <thead>

                        <tr>

                            <th>User name</th>

                            <th>Email</th>

                            <th>Is Approved</th>

                            <th>Delete</th>
                            
                            <th>Settings</th>

                        </tr>

                    </thead>

                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>

                    <td><%# Eval("UserName") %> </td>

                    <td><%# Eval("Email") %> </td>

                    <td><%# Eval("IsApproved") %> </td>

                    <td>
                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" onclick="DeleteUser('<%# Eval("UserName") %>')">Delete</button>                        
                    </td>
                    <td>
<a href="SettingsPage.aspx?userName=<%# Eval("UserName") %>"> <button type="button" class="btn btn-info btn-sm" data-toggle="modal">Settings</button>                        </a> 

                    </td>

                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</asp:Content>


