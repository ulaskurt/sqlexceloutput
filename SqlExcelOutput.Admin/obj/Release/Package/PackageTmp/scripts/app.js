﻿$("#txtPassword").val("");



function CreateUser() {

    var userName = $("#txtUserName").val();
    var password = $("#txtPassword").val();
    var email = $("#txtEmail").val();

    var dataJsonString = JSON.stringify({ "UserName": userName, "Password": password, "Email": email });

   // alert(dataJsonString);
    $.ajax({
        type: "POST",
        url: "Default.aspx/CreateUser",
        data: dataJsonString,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            location.reload();
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function DeleteUser(userName) {

    var dataJsonString = JSON.stringify({ "UserName": userName });

   // alert(dataJsonString);
    $.ajax({
        type: "POST",
        url: "Default.aspx/DeleteUser",
        data: dataJsonString,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            location.reload();
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function DeleteTemplate(id) {

    var dataJsonString = JSON.stringify({ "Id": id });

   // alert(dataJsonString);
    $.ajax({
        type: "POST",
        url: "SettingsPage.aspx/DeleteTemplate",
        data: dataJsonString,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            location.reload();
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}


function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function SaveSettings() {


    var settings = {};

    settings.host = $("#MainContent_txtHost").val();
    settings.dataBaseName = $("#MainContent_txtDataBaseName").val();
    settings.username = $("#MainContent_txtUserName").val();
    settings.password = $("#MainContent_txtPassword").val();
    settings.emailTo = $("#MainContent_txtEmailTo").val();
    settings.excelTemplateFile = $("#MainContent_txtExcelTemplateFile").val();
    settings.customerName = $("#MainContent_txtCustomerName").val();
    settings.smtpHost = $("#MainContent_txtSmtpHost").val();
    settings.smtpUserName = $("#MainContent_txtSmtpUserName").val();
    settings.smtpPassword = $("#MainContent_txtSmtpPassword").val();
    settings.smtpPort = $("#MainContent_txtSmtpPort").val();
    settings.smtpUseTls = $("#MainContent_chkSmtpUseTls").is(':checked');


    var dataJsonString = JSON.stringify(settings);

    settings.UserName = getParameterByName('userName');


   // alert(dataJsonString);
    $.ajax({
        type: "POST",
        url: "SettingsPage.aspx/SaveSettings",
        data: JSON.stringify({'settings':settings}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            location.reload();
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}



function CreateTemplate() {

    var spName = $("#txtSpName").val();
    var excelFile = $("#txtExcelFile").val();
    var userName = getParameterByName('userName');

    var dataJsonString = JSON.stringify({ "SpName": spName, "ExcelFile": excelFile, "UserName": userName });

   // alert(dataJsonString);
    $.ajax({
        type: "POST",
        url: "SettingsPage.aspx/CreateTemplate",
        data: dataJsonString,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            location.reload();
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}