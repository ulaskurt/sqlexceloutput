﻿<%@ Page Title="" Language="vb" AutoEventWireup="false"  
    CodeBehind="Login.aspx.vb" Inherits="SqlExcelOutput.Admin.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"
			  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
			  crossorigin="anonymous"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link id="stylesheet" href="/scripts/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet">
    <script type="text/javascript" src="/scripts/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="http://ajax.cdnjs.com/ajax/libs/json2/20110223/json2.js"></script>

    <script src="scripts/login.js"></script>
</head>
<body>
    <form id="form1" runat="server">
      
            <div class="row" style="margin-top: 75px;">
                <div class="col-sm-4">
                    <h3>Log In</h3>

                    <div class="form-group">
                        <label>User Name</label>
                        <asp:TextBox ID="UserEmail" runat="server" class="form-control" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                            ControlToValidate="UserEmail"
                            Display="Dynamic"
                            ErrorMessage="Cannot be empty."
                            runat="server" />
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <asp:TextBox ID="UserPass" runat="server" class="form-control" TextMode="Password" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                            ControlToValidate="UserPass"
                            ErrorMessage="Cannot be empty."
                            runat="server" />
                    </div>
                    <div class="form-group">
                        <asp:Button ID="cmdLogin" OnClick="Logon_Click" Text="Log On" runat="server" class="btn btn-default" />

                        <asp:Label ID="Msg" ForeColor="red" runat="server" />

                    </div>

                </div>


            </div>

    </form>
</body>
</html>
