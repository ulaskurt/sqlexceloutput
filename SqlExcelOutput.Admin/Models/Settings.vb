﻿Namespace Models
    Public Class Settings
        Public Sub New()
           
        End Sub


        Property Host As String    
        Property DataBaseName As String
        Property UserName As String
        Property Password As String

        Public Property Id As Integer

        Public Property UserId As Guid

        Public Property EmailTo As String

        Public Property SmtpHost As String

        Public Property SmtpPort As String

        Public Property SmtpUserName As String

        Public Property SmtpPassword As String

        Public Property UseTls As Boolean
        Public Property ExcelTemplateFile As String
        
    End Class
End NameSpace