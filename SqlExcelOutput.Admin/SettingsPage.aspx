﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin.Master" CodeBehind="SettingsPage.aspx.vb" Inherits="SqlExcelOutput.Admin.SettingsPage" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row" style="margin-top: 75px;">

        <div class="col-md-6">

            <fieldset>

                <!-- Form Name -->
                <legend>General Settings</legend>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="selectbasic">Host</label>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtHost" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group" id="divDate">
                    <label class="col-md-4 control-label" for="textinput">Database Name</label>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtDataBaseName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group" id="divStarts">
                    <label class="col-md-4 control-label" for="textinput">User Name</label>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtUserName" runat="server" CssClass="form-control" /></asp:TextBox>
                        
                    </div>
                </div>

                <div class="form-group" id="divStarts">
                    <label class="col-md-4 control-label" for="textinput">Password</label>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" /></asp:TextBox>
                        
                    </div>
                </div>


                <div class="form-group" id="divId">
                    <label class="col-md-4 control-label" for="textinput">Email to</label>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtEmailTo" runat="server" CssClass="form-control"></asp:TextBox>

                    </div>
                </div>

                <div class="form-group" id="divId">
                    <label class="col-md-4 control-label" for="textinput">Excel Template File</label>
                    <div class="col-md-4">

                        <asp:TextBox ID="txtExcelTemplateFile" runat="server" CssClass="form-control"></asp:TextBox>

                    </div>
                </div>

                <div class="form-group" id="divId">
                    <label class="col-md-4 control-label" for="textinput">Customer Name</label>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtCustomerName" runat="server" CssClass="form-control"></asp:TextBox>

                    </div>
                </div>

            </fieldset>
        </div>

        <div class="col-md-6">

            <fieldset>

                <!-- Form Name -->
                <legend>Smtp Settings</legend>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="selectbasic">Smtp Host</label>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtSmtpHost" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group" id="divDate">
                    <label class="col-md-4 control-label" for="textinput">Smtp Username</label>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtSmtpUserName" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group" id="divStarts">
                    <label class="col-md-4 control-label" for="textinput">Smtp Password</label>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtSmtpPassword" CssClass="form-control" runat="server"></asp:TextBox>

                    </div>
                </div>

                <div class="form-group" id="divStarts">
                    <label class="col-md-4 control-label" for="textinput">Smtp Port</label>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtSmtpPort" CssClass="form-control" runat="server"></asp:TextBox>

                    </div>
                </div>


                <div class="form-group" id="divId">
                    <label class="col-md-4 control-label" for="textinput">SmtpUse Tls</label>
                    <div class="col-md-4">
                        <asp:CheckBox ID="chkSmtpUseTls" runat="server" CssClass="form-control"></asp:CheckBox>

                    </div>
                </div>
                <button type="button" onclick="SaveSettings();"
                    text="Send as Excel" class="btn btn-default">
                    Save Settings</button>


            </fieldset>
        </div>


    </div>
    <div class="row" style="margin-top: 75px;">
           
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Template</h4>
                    </div>
                    <div class="modal-body">
                        <fieldset>
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Sp name</label>
                                <div class="col-md-4">
                                    <input id="txtSpName" name="textinput"
                                        class="form-control input-md" type="text">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="passwordinput">Excel File</label>
                                <div class="col-md-4">
                                    <input id="txtExcelFile" class="form-control input-md">
                                </div>
                            </div>



                        </fieldset>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="CreateTemplate()">Save</button>
                    </div>
                </div>

            </div>
        </div>
        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">Add new template</button>

        <fieldset>

                <!-- Form Name -->
                <legend>Specific templates</legend>

        <asp:Repeater ID="rptTemplates" runat="server">
            <HeaderTemplate>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Sp Name</th>
                            <th>Excel File</th>                            
                            <th>Delete</th>                            
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("SpName") %> </td>
                    <td><%# Eval("ExcelFile") %> </td>
                    
                    <td>
                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" onclick="DeleteTemplate('<%# Eval("Id") %>')">Delete</button>
                    </td>
                    
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
            <asp:Panel runat="server" id="pnlNoData">
                You do not have sp specific templates
            </asp:Panel>
           </fieldset>
    </div>
</asp:Content>
